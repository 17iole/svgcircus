	var clock=100;
    var countdownId = 0;
    primer=true;
    function start() {
        //Start clock
        clock=100;
        countdownId = setInterval( function(){countdown()}, 50);
    }
    function countdown(){
        if(clock > 0){
            $("#barravida").css("width", clock+"%");
            clock=clock-0.1;
            console.log("clock="+clock);
        }
        else{
            clearInterval(window.countdownId);
            console.log("clock="+clock);
            win();
        }
    }
    function win(){
    	tj.stop();
    	tw= new TimelineMax();
    	tw.to($("#noi"), 1, {rotation:0, transformOrigin:"50% 100%"}, "capamunt")
		var svgg = Snap("#svg");
		var noi = svgg.select("#noi"), cama1 = svgg.select("#cama1"),brac2=svgg.select("#brac2"), brac1=svgg.select("#brac1");
		console.log("YOU WIN! -----------+clock= "+clock);
		$.mbAudio.play('effectSprite', 'aplausos');
		noi.animate({transform:"t100 50"}, 300, function(){
			brac2.animate({transform: "r-20 643 300"}, 400)
			brac1.animate({transform: "r20 643 300"}, 400, function(){
				brac2.animate({transform: "r30 643 300"}, 400)
				brac1.animate({transform: "r-30 643 300"}, 400)
				cama1.animate({transform: "r30 643 300"}, 400, function(){
					brac2.animate({transform: "r-20 643 300"}, 400)
					brac1.animate({transform: "r20 643 300"}, 400, function(){
						$("#jocequilibrista").remove();
						$("#a_dreta").remove();
				    	$("#a_esquerra").remove();
				    	$("#barravida").remove();
				    	back.remove();
						inicimenu=2;
        				Snap.load("svg/main_menu.svg", animacioNen);
					})
					});
				});
			})
	}
    function onJoc1Loaded(data){
    var countdownId = 0;
    if(window.countdownId){
    	clearInterval(window.countdownId);
    }
    
    Snap.load("svg/joc-equilibrista-mbe.svg");
	var svgg = Snap("#svg");
	svgg.append( data );
	var rapidesa=3;
	var ready=0;var ready2=0;

	 focus1 = svgg.select("#focus1"), focus2 = svgg.select("#focus2"), noicostat = svgg.select("#noicostat"), fons=svgg.select("#fons_1_"), ferros=svgg.select("#ferros"),
	$noi = $("#noi"),$focus1 = $("#focus1"),$focus2 = $("#focus2"),$cama1 = $("#cama1"),$cama2 = $("#cama2"),$brac2 = $("#brac2"),$brac1 = $("#brac1"),$cap = $("#cap"),$estrelles=$("#estrelles"),$dressed= $("#a_dressed"),$play= $("#a_play"),
	/*PARTS DEL NOICOSTAT*/
	$cama1_ = $("#cama1_1_"),$cama2_ = $("#cama2_1_"),$brac2_ = $("#brac2_1_"),$brac1_ = $("#brac1_1_"),$cap_ = $("#cap_1_"),$noicostat=$("#noicostat"),brac2=svgg.select("#brac2"),brac1=svgg.select("#brac1"),cama1=svgg.select("#cama1"),cama2=svgg.select("#cama2"),
	vestirVariables(barret, samarreta, pantalons, sabates);
	camina();
	noicostat.attr({transform:"t-700 0"});
	noicostat.animate({transform:"t90 00"}, 3000, pujaescala);
	focus1.attr({transform:"t-180 160", opacity:0});
	focus2.attr({transform:"t-180 160", opacity:0});

	var vida=100;
	var barravida = document.createElement('span');
	barravida.id = "barravida";
	document.body.appendChild(barravida);

	var back = document.createElement('a');
	back.id="back";
	back.href="javascript:void(0);";
	document.getElementById("botonsdedalt").appendChild(back);

	//crear div INFO
    var iDiv = document.createElement('span');
    iDiv.id = 'instruccionsjoc';
    iSpan = document.createElement('div');
    iSpan.id = 'instruccionsgroc';
    document.getElementById('botonsdedalt').appendChild(iSpan);
    document.getElementById('instruccionsgroc').appendChild(iDiv);
    iDiv.innerHTML="Tap at left / right of screen to prevent the man from falling.<br><span id='start'>START!<span>";
    $("#instruccionsjoc").click(function() {
    	$("#instruccionsjoc").remove();
    	ready2=1;
    		if(ready==1){
    		start();
    		joc();
			
    	}
	});

	var a_dreta = document.createElement('a');
	a_dreta.href = "javascript:void(0);";
	a_dreta.id = "a_dreta";
	document.body.appendChild(a_dreta);

	var a_esquerra = document.createElement('a');
	a_esquerra.href = "javascript:void(0);";
	a_esquerra.id = "a_esquerra";
	document.body.appendChild(a_esquerra);

//ANIMACIÓ DONE
    $done=$("#a_done");
    $jocdomador=$("#jocdomador");
    $done.click(function(){
        $jocdomador.remove();
        $("#a_done").css({'display' : 'none'});
        inicimenu=2;
        Snap.load("svg/main_menu.svg", animacioNen);
        });

	function camina(){
	tc = new TimelineMax({repeat:1.5, yoyo:true});
		tc.to($cama1_, 0.5, {rotation:30, ease:Linear.easeNone, transformOrigin: "640 485"}) 
		tc.to($cama2_, 0.5, {rotation:-30, ease:Linear.easeNone, transformOrigin: "640 485"}, "-=0.5") 
		tc.to($brac1_, 0.5, {rotation:40, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.5") 
		tc.to($brac2_, 0.5, {rotation:-40, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.5") 

		tc.to($cama1_, 0.5, {rotation:-30, ease:Linear.easeNone, transformOrigin: "640 485"}) 
		tc.to($cama2_, 0.5, {rotation:30, ease:Linear.easeNone, transformOrigin: "640 485"}, "-=0.5")
		tc.to($brac1_, 0.5, {rotation:-40, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.5")
		tc.to($brac2_, 0.5, {rotation:40, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.5") 
	}
	
	$("#a_dreta").click(function(){
		console.log("dreta");
		if((randi==true)&&(clock>0)){
			tj.stop();
			cama1.animate({transform:"r1"},rapidesa*1000, joc())
			tj.gotoAndPlay("inici");
			rapidesa=rapidesa-0.1;
			console.log(rapidesa);
		}
		if((randi==true)&&(clock==0)){
			win();
		}
	});
	$("#a_esquerra").click(function(){
		console.log("esquerra");
		if((randi==false)&&(clock>0)){
			tj.stop();
			cama1.animate({transform:"r-1"},rapidesa*1000, joc())
			tj.gotoAndPlay("inici");
			rapidesa=rapidesa-(rapidesa*0.05);
			console.log(rapidesa);
			}
		if((randi==false)&&(clock==0)){
			win();
		}
	});
	var randi
	var gr;
	function joc(){
		tj = new TimelineMax({onComplete:loose});
		randi = !! Math.round(Math.random() * 1);
		console.log(randi+ "joc()------------");
		console.log("clock= "+clock);
		if(clock>0){
			if(randi==true){
				tj.gotoAndPlay("capavall");
				gr=1;
			}
			if(randi==false){
				tj.gotoAndPlay("capavall");
				gr=-1;
			}
		}else{
			console.log("YOU WIN!");
			win();
		}
	tj.addLabel("inici");
	tj.to($noi, rapidesa, {rotation:0, transformOrigin:"50% 100%"}, "capamunt")
	tj.to($noi, rapidesa, {ease:Circ.easeIn, rotation:90*gr, transformOrigin:"50% 100%"}, "capavall")
	}
	function loose(){
		tj.stop();
		$("#a_dreta").remove();
		$("#a_esquerra").remove();
		back.remove();
		$.mbAudio.play('effectSprite', 'fail');
		tlo=new TimelineMax({onComplete:gotoMainMenu});
		tlo.to($noi, 3, {ease:Circ.easeOut, rotation:180*gr, transformOrigin:"50% 100%"}, "lose")	
		tlo.to($noi, 4, {ease:Circ.easeOut, rotation:181*gr, transformOrigin:"50% 100%"}, "lose")
			
		clearInterval(window.countdownId);
		clock=0;
        inicimenu=2;
        
	}
	function gotoMainMenu(){
		$("#jocequilibrista").remove();
		$("#barravida").remove();
		Snap.load("svg/main_menu.svg", animacioNen);
	}
	back.onclick = function (){
		tj.stop();
		$("#a_dreta").remove();
		$("#a_esquerra").remove();
		$("#jocequilibrista").remove();
		$("#barravida").remove();
		back.remove();
		clock=0;
		clearInterval(window.countdownId);
        inicimenu=2;
        Snap.load("svg/main_menu.svg", animacioNen);

	}

	function pujaescala(){
		noicostat.animate({transform:"t-120 -160"}, 4000, function(){
			brac2.attr({transform: "r80 643 300"})
			brac1.attr({transform: "r-80 643 300"})

			noicostat.animate({transform:"t-60 -210"}, 300, function(){
				$noi.css("display", "block");
				noicostat.attr({opacity:"0"})
				cama1.attr({transform: "r-3 643 300"})
				cama2.attr({transform: "r3 643 300"})
				noi.attr({transform:"t-55 -65", opacity:"1"})
				brac2.animate({transform: "r0 643 300"}, 300)
				brac1.animate({transform: "r0 643 300"}, 300, function(){
				cama1.animate({transform: "r-23 643 300"}, 300, function(){
					cama1.animate({transform: "r3 643 300"}, 300)
					noi.animate({transform:"t-43 -50"}, 500, function(){
						cama1.animate({transform: "r-23 643 300"}, 300, function(){
							cama1.animate({transform: "r-3 643 300"}, 300)
							cama2.animate({transform: "r3 643 300"}, 300)
							noi.animate({transform:"t-10 -25"}, 500, function(){
								cama1.animate({transform: "r-23 643 300"}, 300, function(){
									cama1.animate({transform: "r-3 643 300"}, 300)
									noi.animate({transform:"t0 0"}, 500, function(){
										ready=1;
										if(ready2==1){
											start();
											joc();
											}
									})
								})
							})
							focus1.animate({opacity:1}, 4000);
							focus2.attr({opacity:1}, 4000);
							moureFocus();
						})
					})
				})
				

				})
				
			});
		});
		fons.animate({transform:"t-280 160"}, 4000);
		ferros.animate({transform:"t-280 160"}, 4000);
	
	te = new TimelineMax();
	te.to($brac1_, 0.5, {rotation:-100, ease:Linear.easeNone, transformOrigin: "640 445"}) 
	te.to($brac2_, 0.5, {rotation:-0, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.4")
	te.to($cama1_, 0.5, {rotation:-30, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 
	te.to($cama2_, 0.5, {rotation:-110, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 

	te.to($brac1_, 0.5, {rotation:-0, ease:Linear.easeNone, transformOrigin: "640 445"})  
	te.to($brac2_, 0.5, {rotation:-100, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.4") 
	te.to($cama1_, 0.5, {rotation:-110, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 
	te.to($cama2_, 0.5, {rotation:-30, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5")

	te.to($brac1_, 0.5, {rotation:-100, ease:Linear.easeNone, transformOrigin: "640 445"}) 
	te.to($brac2_, 0.5, {rotation:-0, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.4")
	te.to($cama1_, 0.5, {rotation:-30, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 
	te.to($cama2_, 0.5, {rotation:-110, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 

	te.to($brac1_, 0.5, {rotation:-0, ease:Linear.easeNone, transformOrigin: "640 445"})  
	te.to($brac2_, 0.5, {rotation:-100, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.4") 
	te.to($cama1_, 0.5, {rotation:-110, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 
	te.to($cama2_, 0.5, {rotation:-30, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 

	te.to($brac1_, 0.5, {rotation:-100, ease:Linear.easeNone, transformOrigin: "640 445"}) 
	te.to($brac2_, 0.5, {rotation:-0, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.4")
	te.to($cama1_, 0.5, {rotation:-30, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 
	te.to($cama2_, 0.5, {rotation:-110, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 

	te.to($brac1_, 0.5, {rotation:-0, ease:Linear.easeNone, transformOrigin: "640 445"})  
	te.to($brac2_, 0.5, {rotation:-100, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.4") 
	te.to($cama1_, 0.5, {rotation:-110, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 
	te.to($cama2_, 0.5, {rotation:-30, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 

	te.to($brac1_, 0.5, {rotation:-0, ease:Linear.easeNone, transformOrigin: "640 445"})  
	te.to($brac2_, 0.5, {rotation:-0, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.4") 
	te.to($cama1_, 0.5, {rotation:-0, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 
	te.to($cama2_, 0.5, {rotation:-0, ease:Linear.easeNone, transformOrigin: "632 485"}, "-=0.5") 
	}
	function moureFocus(){
	tf = new TimelineMax({yoyo:true, repeat:5});
		tf.to($focus1, 7, {rotation:20, ease:Circ.easeInOut, transformOrigin: "0 0"})
		tf.to($focus2, 7, {rotation:-20, ease:Back.easeInOut, transformOrigin: "1200 0"}, "-=7")
		tf.to($focus1, 7, {rotation:-20, ease:Circ.easeInOut, transformOrigin: "0 0"})
		tf.to($focus2, 7, {rotation:20, ease:Back.easeInOut, transformOrigin: "1200 0"}, "-=7")
	}

}