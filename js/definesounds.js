        /*
         * DEFINE SOUNDS
         */
        
        $.mbAudio.sounds = {
            backgroundSprite: {
                id    : "backgroundSprite",
                ogg   : "sounds/bgndsSprite.ogg",
                mp3   : "sounds/bgndsSprite.mp3",
                //example of sprite
                sprite: {
                    levelIntro: {id: "levelIntro", start: 00, end: 75.5, loop: true}
                }
            },

            effectSprite: {
                id    : "effectSprite",
                ogg   : "sounds/effectsSprite.ogg",
                mp3   : "sounds/effectsSprite.mp3",
                //example of sprite
                sprite: {
                    great        : {id: "great", start: 0, end: 1, loop: false},
                    error         : {id: "error", start: 1.2, end: 1.6, loop: false},
                    lion         : {id: "lion", start: 1.7, end: 5.9, loop: false},
                    aplausos     : {id: "aplausos", start: 6, end: 11.5, loop: false},
                    fail         : {id: "fail", start: 11.6, end: 15.8, loop: false},
                    
                    
                }
            }
        };

        function audioIsReady() {
            setTimeout(function () {
                $('#buttons').fadeIn();
                $("#loading").hide();

                if(isStandAlone || !isDevice)
                    $.mbAudio.play('backgroundSprite', 'levelIntro');

            }, 3000);
        }

        $(document).on("initAudio", function () {
            $.mbAudio.pause('effectSprite', audioIsReady);

            $('#turnonaudio').hide();
            $("#loading").show();
        });