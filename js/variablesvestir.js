function vestirVariables(barret, samarreta, pantalons, sabates){
    //Funció que vesteix el personatge segons les variables que li passes

    console.log("barret="+barret);
    var svgg = Snap("#svg");
    $samarretataronja = $("#samarretataronja");
    $samarretavermella = $("#samarretavermella");
    $tirants = $("#tirants");
    $samarretalila = $("#samarretalila");
    $samarretablanca = $("#samarretablanca");
    $pantalonsamples = $("#pantalonsamples");
    $cabellspallasso = $("#cabellspallasso");
    $gorronegre = $("#gorronegre");
    $gorroverd=$("#gorroverd");
    $pantalonsnegres=$("#pantalonsnegres");
    $pantalonsblancs=("#pantalonsblancs");
    $pantalonsverds=("#pantalonsverds");
    $pantalonscurts=("#pantalonscurts");
    $sabatesgrans=$("#sabatesgrans");
    $sabatesmarrons=$("#sabatesmarrons");
    $sabatesblanques=$("#sabatesblanques");
    cuixa2curt=svgg.select("#cuixa2curt");
    cuixa1curt=svgg.select("#cuixa1curt");
    cabellpallasso=svgg.select("#cabellpallasso");
    cabellnormal=svgg.select("#cabellnormal");
    cuixa1=svgg.select("#cuixa1");
    cuixa2=svgg.select("#cuixa2");
    visera=svgg.select("#visera");
    barretnormal=svgg.select("#barretnormal");
    barretrodo=svgg.select("#barretrodo");
    barretquadrat=svgg.select("#barretquadrat");
    cos=svgg.select("#coss");
    tirants_1=svgg.select("#tirants_1");
    maniga1=svgg.select("#bracc1");
    maniga2=svgg.select("#bracc2");
    botons=svgg.select("#botons");
    cuixa1ample=svgg.select("#cuixa1_ample");
    cuixa2ample=svgg.select("#cuixa2_ample");
    panxa=svgg.select("#panxa");
    peu1=svgg.select("#peu1_1");
    peu2=svgg.select("#peu2_1");

    cuixa2curt_=svgg.select("#cuixa2ccurt");
    cuixa1curt_=svgg.select("#cuixa1ccurt");
    cabellpallasso_=svgg.select("#cabellpallasso_1_");
    cabellnormal_=svgg.select("#cabellnormal_1_");
    cuixa1_=svgg.select("#cuixa1_1_");
    cuixa2_=svgg.select("#cuixa2_1_");
    visera_=svgg.select("#visera_1_");
    barretnormal_=svgg.select("#barretnormal_1_");
    barretrodo_=svgg.select("#barretrodo_1_");
    barretquadrat_=svgg.select("#barretquadrat_1_");
    cos_=svgg.select("#coss_1_");
    maniga1_=svgg.select("#bracc2_1_");
    maniga2_=svgg.select("#bracc2_2_");
    botons_=svgg.select("#botons_1_");
    cuixa1ample_=svgg.select("#cuixa1_ample_1_");
    cuixa2ample_=svgg.select("#cuixa2_ample_1_");
    panxa_=svgg.select("#panxa_1_");
    peu1_=svgg.select("#peu1_2_");
    peu2_=svgg.select("#peu2_2_");
    
    switch(barret){
        case 1:
            cabellnormal.attr({opacity:1})
            barretnormal.attr({fill:"#2e6039", opacity:1})
            barretrodo.attr({fill:"rgb(46, 96, 57)", opacity:1})
            visera.attr({fill:"#2e6039", opacity:1})
            barretquadrat.attr({opacity:0})
            cabellpallasso.attr({opacity:0})
            cabellnormal_.attr({opacity:1})
            barretnormal_.attr({fill:"#2e6039", opacity:1})
            barretrodo_.attr({fill:"rgb(46, 96, 57)", opacity:1})
            visera_.attr({fill:"#2e6039", opacity:1})
            barretquadrat_.attr({opacity:0})
            cabellpallasso_.attr({opacity:0})
            break;
        case 2:
            cabellnormal.attr({opacity:1})
            barretquadrat.attr({fill:"#000000", opacity:1})
            barretrodo.attr({opacity:0})
            visera.attr({opacity:0})
            cabellpallasso.attr({opacity:0})
            cabellnormal_.attr({opacity:1})
            barretquadrat_.attr({fill:"#000000", opacity:1})
            barretrodo_.attr({opacity:0})
            visera_.attr({opacity:0})
            cabellpallasso_.attr({opacity:0})
            break;
        case 3:
            cabellnormal.attr({opacity:0})
            barretnormal.attr({opacity:0});
            barretquadrat.attr({opacity:0})
            cabellpallasso.attr({fill:"rgb(218, 125, 45)", opacity:1})
            cabellnormal_.attr({opacity:0})
            barretnormal_.attr({opacity:0});
            barretquadrat_.attr({opacity:0})
            cabellpallasso_.attr({fill:"rgb(218, 125, 45)", opacity:1})
            break;
        }
    switch(samarreta){
        case 1:
            cos.attr({fill:"#F6DDA2"});
            maniga1.attr({fill:"#F6DDA2"});
            maniga2.attr({fill:"#F6DDA2"});
            tirants_1.attr({fill:"rgb(41, 41, 99)", opacity:1});
            botons.attr({opacity:0});
            cos_.attr({fill:"#F6DDA2"});
            maniga1_.attr({fill:"#F6DDA2"});
            maniga2_.attr({fill:"#F6DDA2"});
            break;
        case 2:
            cos.attr({fill:"rgb(218, 125, 45)"});
            maniga1.attr({fill:"rgb(218, 125, 45)"});
            maniga2.attr({fill:"rgb(218, 125, 45)"});
            tirants_1.attr({opacity:0});
            botons.attr({opacity:0});
            cos_.attr({fill:"rgb(218, 125, 45)"});
            maniga1_.attr({fill:"rgb(218, 125, 45)"});
            maniga2_.attr({fill:"rgb(218, 125, 45)"});
            break;
        case 3:
            cos.attr({fill:"rgb(192, 36, 42)"});
            maniga1.attr({fill:"rgb(192, 36, 42)"});
            maniga2.attr({fill:"rgb(192, 36, 42)"});
            tirants_1.attr({opacity:0});
            botons.attr({opacity:1});
            cos_.attr({fill:"rgb(192, 36, 42)"});
            maniga1_.attr({fill:"rgb(192, 36, 42)"});
            maniga2_.attr({fill:"rgb(192, 36, 42)"});
            break;
        case 4:
            cos.attr({fill:"#DDDDDD"});
            maniga1.attr({fill:"#DDDDDD"});
            maniga2.attr({fill:"#DDDDDD"});
            tirants_1.attr({opacity:0});
            botons.attr({opacity:0});
            cos_.attr({fill:"#DDDDDD"});
            maniga1_.attr({fill:"#DDDDDD"});
            maniga2_.attr({fill:"#DDDDDD"});
            cuixa2_.attr({fill:"#dddddd", opacity:1})
            break;
        case 5:
            cos.attr({fill:"rgb(120, 44, 140)"});
            tirants_1.attr({opacity:0});
            maniga1.attr({fill:"rgb(120, 44, 140)"});
            maniga2.attr({fill:"rgb(120, 44, 140)"});
            botons.attr({opacity:0});
            cos_.attr({fill:"rgb(120, 44, 140)"});
            maniga1_.attr({fill:"rgb(120, 44, 140)"});
            maniga2_.attr({fill:"rgb(120, 44, 140)"});
            break;
        }
    switch(pantalons){
        case 1:
            cuixa2.attr({fill:"#F6DDA2", opacity:1})
            cuixa1.attr({fill:"#F6DDA2", opacity:1})
            cuixa1curt.attr({fill:"#292963", opacity:1})
            cuixa2curt.attr({fill:"#292963", opacity:1})
            cuixa2ample.attr({opacity:0})
            cuixa1ample.attr({opacity:0})
            panxa.attr({fill:"#292963"})
            cuixa2_.attr({fill:"#F6DDA2", opacity:1})
            cuixa1_.attr({fill:"#F6DDA2", opacity:1})
            cuixa1curt_.attr({fill:"#292963", opacity:1})
            cuixa2curt_.attr({fill:"#292963", opacity:1})
            cuixa2ample_.attr({opacity:0})
            cuixa1ample_.attr({opacity:0})
            panxa_.attr({fill:"#292963"})
            break;
        case 2:
            cuixa2.attr({opacity:0})
            cuixa1.attr({opacity:0})
            cuixa1curt.attr({opacity:0})
            cuixa2curt.attr({opacity:0})
            cuixa2ample.attr({fill:"rgb(46, 96, 57)", opacity:1})
            cuixa1ample.attr({fill:"rgb(46, 96, 57)", opacity:1})
            panxa.attr({fill:"rgb(46, 96, 57)"})
            cuixa2_.attr({opacity:0})
            cuixa1_.attr({opacity:0})
            cuixa1curt_.attr({opacity:0})
            cuixa2curt_.attr({opacity:0})
            cuixa2ample_.attr({fill:"rgb(46, 96, 57)", opacity:1})
            cuixa1ample_.attr({fill:"rgb(46, 96, 57)", opacity:1})
            panxa_.attr({fill:"rgb(46, 96, 57)"})
            break;
        case 3:
            cuixa2.attr({fill:"#000000", opacity:1})
            cuixa1.attr({fill:"#000000", opacity:1})
            cuixa2ample.attr({opacity:0})
            cuixa1ample.attr({opacity:0})
            cuixa1curt.attr({opacity:0})
            cuixa2curt.attr({opacity:0})
            panxa.attr({fill:"#000000"})
            cuixa2_.attr({fill:"#000000", opacity:1})
            cuixa1_.attr({fill:"#000000", opacity:1})
            cuixa2ample_.attr({opacity:0})
            cuixa1ample_.attr({opacity:0})
            cuixa1curt_.attr({opacity:0})
            cuixa2curt_.attr({opacity:0})
            panxa_.attr({fill:"#000000"})
            break;
        case 4:
            cuixa2.attr({fill:"#dddddd", opacity:1})
            cuixa1.attr({fill:"#dddddd", opacity:1})
            cuixa2ample.attr({opacity:0})
            cuixa1ample.attr({opacity:0})
            cuixa1curt.attr({opacity:0})
            cuixa2curt.attr({opacity:0})
            panxa.attr({fill:"#dddddd"})
            cuixa1_.attr({fill:"#dddddd", opacity:1})
            cuixa2ample_.attr({opacity:0})
            cuixa1ample_.attr({opacity:0})
            cuixa1curt_.attr({opacity:0})
            cuixa2curt_.attr({opacity:0})
            panxa_.attr({fill:"#dddddd"})
            break;
        case 5:
            cuixa2.attr({fill:"rgb(46, 96, 57)", opacity:1})
            cuixa1.attr({fill:"rgb(46, 96, 57)", opacity:1})
            cuixa2ample.attr({opacity:0})
            cuixa1ample.attr({opacity:0})
            cuixa1curt.attr({opacity:0})
            cuixa2curt.attr({opacity:0})
            panxa.attr({fill:"rgb(46, 96, 57)"})
            cuixa2_.attr({fill:"rgb(46, 96, 57)", opacity:1})
            cuixa1_.attr({fill:"rgb(46, 96, 57)", opacity:1})
            cuixa2ample_.attr({opacity:0})
            cuixa1ample_.attr({opacity:0})
            cuixa1curt_.attr({opacity:0})
            cuixa2curt_.attr({opacity:0})
            panxa_.attr({fill:"rgb(46, 96, 57)"})
            break;
        }
    switch(sabates){
        case 1:
            peu2.attr({fill:"#000000", transform:"s2 2, 635 541"});
            peu1.attr({fill:"#000000", transform:"s2 2, 625 541"});
            peu2_.attr({fill:"#000000", transform:"s2 2, 627 541"});
            peu1_.attr({fill:"#000000", transform:"s2 2, 622 541"});
            break;
        case 2:
            peu2.attr({fill:"#825B2C", transform:"s1 1, 635 541"});
            peu1.attr({fill:"#825B2C", transform:"s1 1, 625 541"});
            peu2_.attr({fill:"#825B2C", transform:"s1 1, 635 541"});
            peu1_.attr({fill:"#825B2C", transform:"s1 1, 625 541"});
            break;
        case 3:
            peu2.attr({fill:"#cccccc", transform:"s1 1, 635 541"});
            peu1.attr({fill:"#cccccc", transform:"s1 1, 625 541"});
            peu2_.attr({fill:"#cccccc", transform:"s1 1, 635 541"});
            peu1_.attr({fill:"#cccccc", transform:"s1 1, 625 541"});
            break;
    }
}