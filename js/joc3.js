function onJoc3Loaded( data ){ 
	a=2;
	window.s = Snap("#svg");
    s.append( data );
    var noi=s.select("#noi");
    noicostat=s.select("#noicostat");
    noicostat.attr({opacity:0});
    var pecesposades=0;
    $noi = $("#noi"),
	$cama1 = $("#cama1");
	$cama2 = $("#cama2");
	$brac2 = $("#brac2");
	$brac1 = $("#brac1");
	$cap = $("#cap");
	$estrelles=$("#estrelles");
	$dressed2= $("#getdressed");
	$play2=$("#play_3_");
	$cama1_ = $("#cama1_1_");
	$cama2_ = $("#cama2_1_");
	$brac2_ = $("#brac2_1_");
	$brac1_ = $("#brac1_1_");
	$cap_ = $("#cap_1_");
	$noicostat=$("#noicostat");
    vestirVariables(barret, samarreta, pantalons, sabates);

    var back = document.createElement('a');
    back.id="back";
    back.href="javascript:void(0);";
    document.getElementById("botonsdedalt").appendChild(back);

    //mirar si hi ha 2 svg
    var svgs = document.getElementsByTagName('svg');
	if(svgs.length >1) {
	    svgs[1].remove();
	}

    $jocdomador=$("#jocdomador");

	tt = new TimelineMax();
    Draggable.create("#globus", {
        onDragEnd:function() {
        	console.log("x= "+this.x+" y= "+this.y);
            if(((this.x<710)&&(this.x>675))&&((this.y>15)&&(this.y<50))){
            	$("#globus2").css("display", "block");
            	$("#globus").css("display", "none");
            	pecesposades++;console.log(pecesposades);guanyar();$.mbAudio.play('effectSprite', 'great');
            }else{
            	tt.to($("#globus"), 0.2, {x:0, y:0});
            }
        },
        dragResistance :-0.3
    });
    Draggable.create("#monocicle", {
        dragResistance :-0.3,
        onDragEnd:function() {
        	console.log("x= "+this.x+" y= "+this.y);
            if(((this.x<915)&&(this.x>870))&&((this.y>-250)&&(this.y<-190))){
            	$("#monocicle2").css("display", "block");
            	$("#monocicle").css("display", "none");
            	pecesposades++;
            	console.log(pecesposades);guanyar();$.mbAudio.play('effectSprite', 'great');
            }else{
            	tt.to($("#monocicle"), 0.2, {x:0, y:0});
            }
        }
    });
    Draggable.create("#pilotagran", {
        dragResistance :-0.3,
        onDragEnd:function() {
        	console.log("x= "+this.x+" y= "+this.y);
            if(((this.x<618)&&(this.x>580))&&((this.y>107)&&(this.y<137))){
            	$("#pilotagran2").css("display", "block");
            	$("#pilotagran").css("display", "none");
            	$.mbAudio.play('effectSprite', 'great');
                pecesposades++;
                guanyar();
            }else{
            	tt.to($("#pilotagran"), 0.2, {x:0, y:0});
            }
        }
    });
    Draggable.create("#pilotapetita", {
        dragResistance :-0.3,
        onDragEnd:function() {
        	console.log("x= "+this.x+" y= "+this.y);
            if(((this.x<441)&&(this.x>423))&&((this.y>-189)&&(this.y<-155))){
            	$("#pilotapetita2").css("display", "block");
            	$("#pilotapetita").css("display", "none");
                $.mbAudio.play('effectSprite', 'great');
            	pecesposades++;
                guanyar();
            }else{
            	tt.to($("#pilotapetita"), 0.2, {x:0, y:0});
            }
        }
    });
    Draggable.create("#bitlla", {
        dragResistance :-0.3,
        onDragEnd:function() {
        	console.log("x= "+this.x+" y= "+this.y);
            if(((this.x<738)&&(this.x>723))&&((this.y>19)&&(this.y<44))){
            	$("#bitlla2").css("display", "block");
            	$("#bitlla").css("display", "none");
            	$.mbAudio.play('effectSprite', 'great');
                pecesposades++;
                guanyar();
            }else{
            	tt.to($("#bitlla"), 0.2, {x:0, y:0});
            }
        }
    });
    Draggable.create("#conill", {
        dragResistance :-0.3,
        onDragEnd:function() {
        	console.log("x= "+this.x+" y= "+this.y);
            if(((this.x<676)&&(this.x>643))&&((this.y>-130)&&(this.y<-100))){
            	$("#conill2").css("display", "block");
            	$("#conill").css("display", "none");
                $.mbAudio.play('effectSprite', 'great');
            	pecesposades++;
                guanyar();
            }else{
            	tt.to($("#conill"), 0.2, {x:0, y:0});
            }
        }
    });
    Draggable.create("#estrella", {
        dragResistance :-0.3,
        onDragEnd:function() {
        	console.log("x= "+this.x+" y= "+this.y);
            if(((this.x<880)&&(this.x>853))&&((this.y>-418)&&(this.y<-383))){
            	$("#estrella2").css("display", "block");
            	$("#estrella").css("display", "none");
                $.mbAudio.play('effectSprite', 'great');
            	pecesposades++;
                guanyar();
            }else{
            	tt.to($("#estrella"), 0.2, {x:0, y:0});
            }
        }
    });
function guanyar(){
    if(pecesposades==7){
        setTimeout(function(){$.mbAudio.play('effectSprite', 'aplausos');}, 1);
        
    	tl = new TimelineMax({repeat:1, onComplete:marxar});
	    tl.to($brac2, 0.3, {rotation:30, ease:Sine.easeInOut, transformOrigin: "640 460"}) 
	    tl.to($brac2, 0.3, {rotation:-30, ease:Sine.easeInOut}) 
	    tl.to($brac2, 0.3, {rotation:0, ease:Sine.easeInOut}) 
	    tl.to($brac1, 0.3, {rotation:30, ease:Sine.easeInOut, transformOrigin: "630 465"}, "-=0.9") 
	    tl.to($brac1, 0.3, {rotation:-30, ease:Sine.easeInOut}, "-=0.6") 
	    tl.to($brac1, 0.3, {rotation:0, ease:Sine.easeInOut}, "-=0.3") 
	    tl.to($cap, 0.3, {rotation:-10, ease:Sine.easeInOut, transformOrigin: "640 465"}, "-=0.6") 
	    tl.to($cap, 0.3, {rotation:10, ease:Sine.easeInOut}, "-=0.3") 
	    tl.to($cap, 0.3, {rotation:0, ease:Sine.easeInOut}, "-=0") 
	    tl.to($cama2, 0.3, {rotation:-50, ease:Sine.easeInOut, transformOrigin: "640 485"}, "-=1.2") 
	    tl.to($cama2, 0.3, {rotation:0, ease:Sine.easeInOut}, "-=0.6") 

	}
}
function marxar(){
	noi.attr({opacity:0});
	noicostat.attr({opacity:1, transform:"s-1 1"});
    tc = new TimelineMax({repeat:3, yoyo:true, onComplete:returntoMenu });
    tc.to($cama1_, 0.5, {rotation:30, ease:Linear.easeNone, transformOrigin: "640 485"}) 
    tc.to($cama2_, 0.5, {rotation:-30, ease:Linear.easeNone, transformOrigin: "640 485"}, "-=0.5") 
    tc.to($brac1_, 0.5, {rotation:40, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.5") 
    tc.to($brac2_, 0.5, {rotation:-40, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.5")
    tc.to($cama1_, 0.5, {rotation:-30, ease:Linear.easeNone, transformOrigin: "640 485"})
    tc.to($cama2_, 0.5, {rotation:30, ease:Linear.easeNone, transformOrigin: "640 485"}, "-=0.5")
    tc.to($brac1_, 0.5, {rotation:-40, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.5")
    tc.to($brac2_, 0.5, {rotation:40, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.5")

	noicostat.animate({transform:"s-1 1 t750 0"}, 2500);
    
}
function returntoMenu(){
    inicimenu=2;
    $("#jocpallasso").remove(); 
    Snap.load("svg/main_menu.svg", animacioNen);
}
back.onclick = function (){
        marxar();

        setTimeout(function(){
            $("#jocpallasso").remove();
            back.remove();
            inicimenu=2;
            Snap.load("svg/main_menu.svg", animacioNen);
        }, 3);
        
    }
}

