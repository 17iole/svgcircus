

function onVestirLoaded(data){
    Snap.load("svg/getdressed.svg");
	var svgg = Snap("#svg");
	svgg.append( data );
    $getdressed=$("#getdressed");
    var svgs = document.getElementsByTagName('svg');
    if(svgs.length >1) {
        svgs[1].remove();
    }
	var getdressed =svgg.select("#getdressed");
	svgg.animate({opacity : 1}, 500,mina.easeout );
	noi = svgg.select("#noi");
	armari = svgg.select("#armari");
	calaix1 = svgg.select("#calaix1");
	calaix2 = svgg.select("#calaix2");
	calaix3 = svgg.select("#calaix3");
	calaix4 = svgg.select("#calaix4");
	fustacalaix1 = svgg.select("#fustacalaix1"),
	fustacalaix2 = svgg.select("#fustacalaix2"),
	fustacalaix3 = svgg.select("#fustacalaix3"),
	fustacalaix4 = svgg.select("#fustacalaix4");
	noicostat = svgg.select("#noicostat");
    $cama1_ = $("#cama1_1_");
    $cama2_ = $("#cama2_1_");
    $brac2_ = $("#brac2_1_");
    $brac1_ = $("#brac1_1_");
    $cap_ = $("#cap_1_");
    $noicostat=$("#noicostat");

    $("#a_done").css({'display' : 'block'});

    vestirVariables(barret, samarreta, pantalons, sabates);
    
    //ANIMACIÓ INICIAL NOI COSTAT
	noi.animate({opacity: 0},1);
	noicostat.animate({opacity: 1, transform:"t700 0 s-1,1"},1, function(){
		noicostat.animate({transform:"t250 0 s-1,1"}, 2000, mina.linear, function(){
			noicostat.animate({opacity: 0}, 1);
			noi.animate({opacity: 1, transform:"t250 0"},1);
		});
	
	});
	tc = new TimelineMax({repeat:4, yoyo:true});
	tc.to($cama1_, 0.5, {rotation:30, ease:Linear.easeNone, transformOrigin: "640 485"}) 
	tc.to($cama2_, 0.5, {rotation:-30, ease:Linear.easeNone, transformOrigin: "640 485"}, "-=0.5") 
	tc.to($brac1_, 0.5, {rotation:40, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.5") 
	tc.to($brac2_, 0.5, {rotation:-40, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.5") 

	tc.to($cama1_, 0.5, {rotation:-30, ease:Linear.easeNone, transformOrigin: "640 485"}) 
	tc.to($cama2_, 0.5, {rotation:30, ease:Linear.easeNone, transformOrigin: "640 485"}, "-=0.5")
	tc.to($brac1_, 0.5, {rotation:-40, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.5")
	tc.to($brac2_, 0.5, {rotation:40, ease:Linear.easeNone, transformOrigin: "640 445"}, "-=0.5") 

    //ANIMACIÓ DONE
    $done=$("#a_done");
    $done.click(function(){
        noi.attr({opacity:0});
        //vestirVariables(barret, samarreta, pantalons, sabates);
        noicostat.animate({transform:"t250 0 s1 1", opacity:1},1, function(){
            tc.restart();
            noicostat.animate({transform:"t750 0 s1 1"}, 2000, mina.linear, function(){
                $getdressed.remove();
                $("#a_done").css({'display' : 'none'});
                inicimenu=1;
                Snap.load("svg/main_menu.svg", animacioNen)
            });
        });
    })

	//OBRIR i TANCAR CALAIXOS
	c1 = false,
	c2 = false,
	c3 = false,
	c4 = false;

	CSSPlugin.defaultTransformPerspective = 1500;

	fustacalaix1.click(function(){ obrirTancar(calaix1, c1, 1);});
	fustacalaix2.click(function(){ obrirTancar(calaix2, c2, 2); });
	fustacalaix3.click(function(){ obrirTancar(calaix3, c3, 3); });
	fustacalaix4.click(function(){ obrirTancar(calaix4, c4, 4); });

	function obrirTancar(calaix, obert, num){
		if(obert==false){
			console.log("ieee "+obert);
			 calaix.animate( { transform: "t" + "270 80" }, 250, mina.easein );
			 switch(num){
			 	case 1:c1=true;break;
			 	case 2:c2=true;break;
			 	case 3:c3=true;break;
			 	case 4:c4=true;break;
			 }
		}else{
			console.log("iooo "+obert);
			calaix.animate( { transform: "t" + "0 0" }, 250, mina.easein );
		 	switch(num){
			 	case 1:c1=false;break;
			 	case 2:c2=false;break;
			 	case 3:c3=false;break;
			 	case 4:c4=false;break;
			 }
		}
	}

	//ARROSSEGAR PECES DE ROBA
	tt = new TimelineMax();
var $samarretataronja = $("#samarretataronja");
	$samarretavermella = $("#samarretavermella");
	$tirants = $("#tirants");
	$samarretalila = $("#samarretalila");
	$samarretablanca = $("#samarretablanca");
	$pantalonsamples = $("#pantalonsamples");
	$cabellspallasso = $("#cabellspallasso");
	$gorronegre = $("#gorronegre");
	$gorroverd=$("#gorroverd");
	$pantalonsnegres=$("#pantalonsnegres");
	$pantalonsblancs=("#pantalonsblancs");
	$pantalonsverds=("#pantalonsverds");
	$pantalonscurts=("#pantalonscurts");
	$sabatesgrans=$("#sabatesgrans");
	$sabatesmarrons=$("#sabatesmarrons");
	$sabatesblanques=$("#sabatesblanques");
	cuixa2curt=svgg.select("#cuixa2curt");
	cuixa1curt=svgg.select("#cuixa1curt");
	cabellpallasso=svgg.select("#cabellpallasso");
	cabellnormal=svgg.select("#cabellnormal");
	cuixa1=svgg.select("#cuixa1");
	cuixa2=svgg.select("#cuixa2");
	visera=svgg.select("#visera");
	barretnormal=svgg.select("#barretnormal");
	barretrodo=svgg.select("#barretrodo");
	barretquadrat=svgg.select("#barretquadrat");
	cos=svgg.select("#coss");
	tirants_1=svgg.select("#tirants_1");
	maniga1=svgg.select("#bracc1");
	maniga2=svgg.select("#bracc2");
	botons=svgg.select("#botons");
	cuixa1ample=svgg.select("#cuixa1_ample");
	cuixa2ample=svgg.select("#cuixa2_ample");
	panxa=svgg.select("#panxa");
	peu1=svgg.select("#peu1_1");
	peu2=svgg.select("#peu2_1");

    cuixa2curt_=svgg.select("#cuixa2ccurt");
    cuixa1curt_=svgg.select("#cuixa1ccurt");
    cabellpallasso_=svgg.select("#cabellpallasso_1_");
    cabellnormal_=svgg.select("#cabellnormal_1_");
    cuixa1_=svgg.select("#cuixa1_1_");
    cuixa2_=svgg.select("#cuixa2_1_");
    visera_=svgg.select("#visera_1_");
    barretnormal_=svgg.select("#barretnormal_1_");
    barretrodo_=svgg.select("#barretrodo_1_");
    barretquadrat_=svgg.select("#barretquadrat_1_");
    cos_=svgg.select("#coss_1_");
    maniga1_=svgg.select("#bracc2_1_");
    maniga2_=svgg.select("#bracc2_2_");
    botons_=svgg.select("#botons_1_");
    cuixa1ample_=svgg.select("#cuixa1_ample_1_");
    cuixa2ample_=svgg.select("#cuixa2_ample_1_");
    panxa_=svgg.select("#panxa_1_");
    peu1_=svgg.select("#peu1_2_");
    peu2_=svgg.select("#peu2_2_");

	//BARRETS -------------------------------------------------------------------------------
	Draggable.create("#gorroverd", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<510)&&(this.x>430))&&((this.y>-15)&&(this.y<50))){
            	barret=1;
            	cabellnormal.attr({opacity:1})
            	barretnormal.attr({fill:"#2e6039", opacity:1})
            	barretrodo.attr({fill:"rgb(46, 96, 57)", opacity:1})
            	visera.attr({fill:"#2e6039", opacity:1})
            	barretquadrat.attr({opacity:0})
            	cabellpallasso.attr({opacity:0}, pecaposada(1))

                cabellnormal_.attr({opacity:1})
                barretnormal_.attr({fill:"#2e6039", opacity:1})
                barretrodo_.attr({fill:"rgb(46, 96, 57)", opacity:1})
                visera_.attr({fill:"#2e6039", opacity:1})
                barretquadrat_.attr({opacity:0})
                cabellpallasso_.attr({opacity:0})
            tt.to($gorroverd, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($gorroverd, 0.2, {x:0, y:0});}
        }
    });
    Draggable.create("#gorronegre", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<435)&&(this.x>345))&&((this.y>-55)&&(this.y<23))){
            	barret=2;
            	cabellnormal.attr({opacity:1})
            	barretquadrat.attr({fill:"#000000", opacity:1})
            	barretrodo.attr({opacity:0})
            	visera.attr({opacity:0})
            	cabellpallasso.attr({opacity:0}, pecaposada(1))

                cabellnormal_.attr({opacity:1})
                barretquadrat_.attr({fill:"#000000", opacity:1})
                barretrodo_.attr({opacity:0})
                visera_.attr({opacity:0})
                cabellpallasso_.attr({opacity:0})
            tt.to($gorronegre, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($gorronegre, 0.2, {x:0, y:0});}
        }
    });
    Draggable.create("#cabellspallasso", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<360)&&(this.x>280))&&((this.y>-33)&&(this.y<45))){
            	barret=3;
            	cabellnormal.attr({opacity:0})
            	barretnormal.attr({opacity:0});
            	barretquadrat.attr({opacity:0})
            	cabellpallasso.attr({fill:"rgb(218, 125, 45)", opacity:1}, pecaposada(1))

                cabellnormal_.attr({opacity:0})
                barretnormal_.attr({opacity:0});
                barretquadrat_.attr({opacity:0})
                cabellpallasso_.attr({fill:"rgb(218, 125, 45)", opacity:1})
            	tt.to($cabellspallasso, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($cabellspallasso, 0.2, {x:0, y:0});}
        }
    });

	//SAMARRETES -----------------------------------------------------------------------------
	Draggable.create("#tirants", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<575)&&(this.x>490))&&((this.y>94)&&(this.y<160))){
            	samarreta=1;
            	cos.attr({fill:"#F6DDA2"});
				maniga1.attr({fill:"#F6DDA2"});
				maniga2.attr({fill:"#F6DDA2"});
				tirants_1.attr({fill:"rgb(41, 41, 99)", opacity:1});
				botons.attr({opacity:0}, pecaposada(2));
                tt.to($tirants, 0.2, {x:0, y:0, delay:-1.5});

                cos_.attr({fill:"#F6DDA2"});
                maniga1_.attr({fill:"#F6DDA2"});
                maniga2_.attr({fill:"#F6DDA2"});
				
            }else{tt.to($tirants, 0.2, {x:0, y:0});}
            console.log("x: "+this.x+"/ y: "+this.y);
        }
    });
    Draggable.create("#samarretataronja", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<530)&&(this.x>430))&&((this.y>80)&&(this.y<130))){
            	samarreta=2;
            	cos.attr({fill:"rgb(218, 125, 45)"});
				maniga1.attr({fill:"rgb(218, 125, 45)"});
				maniga2.attr({fill:"rgb(218, 125, 45)"});
				tirants_1.attr({opacity:0});
				botons.attr({opacity:0}, pecaposada(2));

                cos_.attr({fill:"rgb(218, 125, 45)"});
                maniga1_.attr({fill:"rgb(218, 125, 45)"});
                maniga2_.attr({fill:"rgb(218, 125, 45)"});

				tt.to($samarretataronja, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($samarretataronja, 0.2, {x:0, y:0});}
        }
    }); 
    Draggable.create("#samarretavermella", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<530)&&(this.x>400))&&((this.y>40)&&(this.y<130))){
            	samarreta=3;
            	cos.attr({fill:"rgb(192, 36, 42)"});
				maniga1.attr({fill:"rgb(192, 36, 42)"});
				maniga2.attr({fill:"rgb(192, 36, 42)"});
				tirants_1.attr({opacity:0});
				botons.attr({opacity:1}, pecaposada(2));

                cos_.attr({fill:"rgb(192, 36, 42)"});
                maniga1_.attr({fill:"rgb(192, 36, 42)"});
                maniga2_.attr({fill:"rgb(192, 36, 42)"});
				tt.to($samarretavermella, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($samarretavermella, 0.2, {x:0, y:0});}
        }
    });
    Draggable.create("#samarretablanca", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<410)&&(this.x>320))&&((this.y>23)&&(this.y<120))){
            	samarreta=4;
            	cos.attr({fill:"#DDDDDD"});
				maniga1.attr({fill:"#DDDDDD"});
				maniga2.attr({fill:"#DDDDDD"});
				tirants_1.attr({opacity:0});
				botons.attr({opacity:0}, pecaposada(2));

                cos_.attr({fill:"#DDDDDD"});
                maniga1_.attr({fill:"#DDDDDD"});
                maniga2_.attr({fill:"#DDDDDD"});
				tt.to($samarretablanca, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($samarretablanca, 0.2, {x:0, y:0});}
            console.log("x: "+this.x+"/ y: "+this.y);
        }
    });
    
    Draggable.create("#samarretalila", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<350)&&(this.x>250))&&((this.y>10)&&(this.y<100))){
            	samarreta=5;
            	cos.attr({fill:"rgb(120, 44, 140)"});
            	tirants_1.attr({opacity:0});
				maniga1.attr({fill:"rgb(120, 44, 140)"});
				maniga2.attr({fill:"rgb(120, 44, 140)"});
				botons.attr({opacity:0}, pecaposada(2));

                cos_.attr({fill:"rgb(120, 44, 140)"});
                maniga1_.attr({fill:"rgb(120, 44, 140)"});
                maniga2_.attr({fill:"rgb(120, 44, 140)"});
				tt.to($samarretalila, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($samarretalila, 0.2, {x:0, y:0});}
        }
    });
   

    //PANTALONS ------------------------------------------------------------------------------
    Draggable.create("#pantalonscurts", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<580)&&(this.x>370))&&((this.y>50)&&(this.y<130))){
            	pantalons=1;
            	cuixa2.attr({fill:"#F6DDA2", opacity:1})
            	cuixa1.attr({fill:"#F6DDA2", opacity:1})
            	cuixa1curt.attr({fill:"#292963", opacity:1})
            	cuixa2curt.attr({fill:"#292963", opacity:1})
            	cuixa2ample.attr({opacity:0})
            	cuixa1ample.attr({opacity:0})
            	panxa.attr({fill:"#292963"}, pecaposada(3))

                cuixa2_.attr({fill:"#F6DDA2", opacity:1})
                cuixa1_.attr({fill:"#F6DDA2", opacity:1})
                cuixa1curt_.attr({fill:"#292963", opacity:1})
                cuixa2curt_.attr({fill:"#292963", opacity:1})
                cuixa2ample_.attr({opacity:0})
                cuixa1ample_.attr({opacity:0})
                panxa_.attr({fill:"#292963"})
            	tt.to($pantalonscurts, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($pantalonscurts, 0.2, {x:0, y:0});}

        }
    });
    Draggable.create("#pantalonsamples", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<530)&&(this.x>400))&&((this.y>40)&&(this.y<130))){
            	pantalons=2;
            	cuixa2.attr({opacity:0})
            	cuixa1.attr({opacity:0})
            	cuixa1curt.attr({opacity:0})
            	cuixa2curt.attr({opacity:0})
            	cuixa2ample.attr({fill:"rgb(46, 96, 57)", opacity:1})
            	cuixa1ample.attr({fill:"rgb(46, 96, 57)", opacity:1})
            	panxa.attr({fill:"rgb(46, 96, 57)"}, pecaposada(3))

                cuixa2_.attr({opacity:0})
                cuixa1_.attr({opacity:0})
                cuixa1curt_.attr({opacity:0})
                cuixa2curt_.attr({opacity:0})
                cuixa2ample_.attr({fill:"rgb(46, 96, 57)", opacity:1})
                cuixa1ample_.attr({fill:"rgb(46, 96, 57)", opacity:1})
                panxa_.attr({fill:"rgb(46, 96, 57)"})
            	tt.to($pantalonsamples, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($pantalonsamples, 0.2, {x:0, y:0});}
        }
    });

    Draggable.create("#pantalonsnegres", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<475)&&(this.x>375))&&((this.y>16)&&(this.y<100))){
            	pantalons=3;
            	cuixa2.attr({fill:"#000000", opacity:1})
            	cuixa1.attr({fill:"#000000", opacity:1})
            	cuixa2ample.attr({opacity:0})
            	cuixa1ample.attr({opacity:0})
            	cuixa1curt.attr({opacity:0})
            	cuixa2curt.attr({opacity:0})
            	panxa.attr({fill:"#000000"}, pecaposada(3))

                cuixa2_.attr({fill:"#000000", opacity:1})
                cuixa1_.attr({fill:"#000000", opacity:1})
                cuixa2ample_.attr({opacity:0})
                cuixa1ample_.attr({opacity:0})
                cuixa1curt_.attr({opacity:0})
                cuixa2curt_.attr({opacity:0})
                panxa_.attr({fill:"#000000"})
            	tt.to($pantalonsnegres, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($pantalonsnegres, 0.2, {x:0, y:0});}
            console.log("x: "+this.x+"/ y: "+this.y);
        }
    });
    Draggable.create("#pantalonsblancs", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<420)&&(this.x>320))&&((this.y>0)&&(this.y<80))){
            	pantalons=4;
            	cuixa2.attr({fill:"#dddddd", opacity:1})
            	cuixa1.attr({fill:"#dddddd", opacity:1})
            	cuixa2ample.attr({opacity:0})
            	cuixa1ample.attr({opacity:0})
            	cuixa1curt.attr({opacity:0})
            	cuixa2curt.attr({opacity:0})
            	panxa.attr({fill:"#dddddd"}, pecaposada(3))

                cuixa2_.attr({fill:"#dddddd", opacity:1})
                cuixa1_.attr({fill:"#dddddd", opacity:1})
                cuixa2ample_.attr({opacity:0})
                cuixa1ample_.attr({opacity:0})
                cuixa1curt_.attr({opacity:0})
                cuixa2curt_.attr({opacity:0})
                panxa_.attr({fill:"#dddddd"})
            	tt.to($pantalonsblancs, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($pantalonsblancs, 0.2, {x:0, y:0});}

            console.log("x: "+this.x+"/ y: "+this.y);
        }
    });
    Draggable.create("#pantalonsverds", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<370)&&(this.x>270))&&((this.y>-20)&&(this.y<70))){
            	pantalons=5;
                cuixa2.attr({fill:"rgb(46, 96, 57)", opacity:1})
                cuixa1.attr({fill:"rgb(46, 96, 57)", opacity:1})
                cuixa2ample.attr({opacity:0})
                cuixa1ample.attr({opacity:0})
                cuixa1curt.attr({opacity:0})
                cuixa2curt.attr({opacity:0})
                panxa.attr({fill:"rgb(46, 96, 57)"}, pecaposada(3))

            	cuixa2_.attr({fill:"rgb(46, 96, 57)", opacity:1})
            	cuixa1_.attr({fill:"rgb(46, 96, 57)", opacity:1})
            	cuixa2ample_.attr({opacity:0})
            	cuixa1ample_.attr({opacity:0})
            	cuixa1curt_.attr({opacity:0})
            	cuixa2curt_.attr({opacity:0})
            	panxa_.attr({fill:"rgb(46, 96, 57)"})
            	tt.to($pantalonsverds, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($pantalonsverds, 0.2, {x:0, y:0});}
        }
    });
    

    //SABATES -----------------------------------------------------------------------------------------	
    Draggable.create("#sabatesgrans", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<500)&&(this.x>400))&&((this.y>0)&&(this.y<50))){
            	sabates=1;
            	peu2.attr({fill:"#000000"});
            	peu1.attr({fill:"#000000"});
            	peu2.animate({transform:"s2 2, 635 541"}, 1000, mina.bounce);
            	peu1.animate({transform:"s2 2, 625 541"}, 1000, mina.bounce, pecaposada(3));

                peu2_.attr({fill:"#000000", transform:"s2 2, 627 541"});
                peu1_.attr({fill:"#000000", transform:"s2 2, 622 541"});
            	tt.to($sabatesgrans, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($sabatesgrans, 0.2, {x:0, y:0});}
        console.log("x: "+this.x+"/ y: "+this.y);
    	}
    });
    Draggable.create("#sabatesmarrons", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<430)&&(this.x>330))&&((this.y>-15)&&(this.y<44))){
            	sabates=2;
            	peu2.attr({fill:"#825B2C"});
            	peu1.attr({fill:"#825B2C"});
            	peu2.animate({transform:"s1 1, 635 541"}, 1000, mina.bounce);
            	peu1.animate({transform:"s1 1, 625 541"}, 1000, mina.bounce, pecaposada(3));

                peu2_.attr({fill:"#825B2C", transform:"s1 1, 635 541"});
                peu1_.attr({fill:"#825B2C", transform:"s1 1, 625 541"});
            	tt.to($sabatesmarrons, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($sabatesmarrons, 0.2, {x:0, y:0});}
        console.log("x: "+this.x+"/ y: "+this.y);
    	}
    });
    Draggable.create("#sabatesblanques", {
        dragResistance :-0.3,
        onDragEnd:function() {
            if(((this.x<390)&&(this.x>280))&&((this.y>-20)&&(this.y<30))){
            	sabates=3;
            	peu2.attr({fill:"#cccccc"});
            	peu1.attr({fill:"#cccccc"});
            	peu2.animate({transform:"s1 1, 635 541"}, 1000, mina.bounce);
            	peu1.animate({transform:"s1 1, 625 541"}, 1000, mina.bounce, pecaposada(3));

                peu2_.attr({fill:"#cccccc", transform:"s1 1, 635 541"});
                peu1_.attr({fill:"#cccccc", transform:"s1 1, 625 541"});
            	tt.to($sabatesblanques, 0.2, {x:0, y:0, delay:-1.5});
            }else{tt.to($sabatesblanques, 0.2, {x:0, y:0});}
        console.log("x: "+this.x+"/ y: "+this.y);
    	}
    });

    $brac2 = $("#brac2");
	$brac1 = $("#brac1");
	$cama2 = $("#cama2");
	$cama1 = $("#cama1");
	$cap = $("#cap");
    function pecaposada (num){
    	switch(num){
    		case 1:
    			tt.to($cap, 0.3, {rotation:-10, ease:Sine.easeInOut, transformOrigin: "640 465"}) 
			    tt.to($cap, 0.3, {rotation:10, ease:Sine.easeInOut}) 
			    tt.to($cap, 0.3, {rotation:0, ease:Sine.easeInOut}) 
			    break;
    		case 2:
    			tt.to($brac2, 0.3, {rotation:30, ease:Sine.easeInOut, transformOrigin: "640 460"}) 
			    tt.to($brac2, 0.3, {rotation:-30, ease:Sine.easeInOut}) 
			    tt.to($brac2, 0.3, {rotation:0, ease:Sine.easeInOut}) 
			    tt.to($brac1, 0.3, {rotation:30, ease:Sine.easeInOut, transformOrigin: "620 460"}, "-=0.9") 
			    tt.to($brac1, 0.3, {rotation:-30, ease:Sine.easeInOut},"-=0.6") 
			    tt.to($brac1, 0.3, {rotation:0, ease:Sine.easeInOut},"-=0.3");
			    break;
			case 3:
				tt.to($cama2, 0.3, {rotation:-50, ease:Sine.easeInOut, transformOrigin: "640 485"}) 
   		 		tt.to($cama2, 0.3, {rotation:0, ease:Sine.easeInOut}) 
   		 		tt.to($cama1, 0.3, {rotation:50, ease:Sine.easeInOut, transformOrigin: "640 485"}) 
   		 		tt.to($cama1, 0.3, {rotation:0, ease:Sine.easeInOut}) 
    	}
    }
}	